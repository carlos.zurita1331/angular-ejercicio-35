import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Usuario80Component } from './usuario80.component';

describe('Usuario80Component', () => {
  let component: Usuario80Component;
  let fixture: ComponentFixture<Usuario80Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Usuario80Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Usuario80Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
